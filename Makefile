up: docker-up
down: docker-down
build: docker-build
pull: docker-pull
node-build: docker-node-build
node-watch: docker-node-watch


docker-up: 
	docker-compose up -d
docker-down:
	docker-compose down --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

docker-node-build:
	docker-compose exec vue-crm-nodejs npm run build

docker-node-watch:
	docker-compose exec vue-crm-nodejs npm run watch
