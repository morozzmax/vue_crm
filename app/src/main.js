import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import 'materialize-css/dist/js/materialize.min'
import dateFilter from '@/filters/date.filters'
import messagePlugin from '@/utils/message.plugin'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.use(messagePlugin)
Vue.filter('date', dateFilter)

const firebaseConfig = {
  apiKey: "AIzaSyDez_i-lzzDlCkv8Zqcb9ARziqX1gv8tkY",
  authDomain: "vue-crm-a5da9.firebaseapp.com",
  databaseURL: "https://vue-crm-a5da9.firebaseio.com",
  projectId: "vue-crm-a5da9",
  storageBucket: "vue-crm-a5da9.appspot.com",
  messagingSenderId: "776332179188",
  appId: "1:776332179188:web:b5aa2d616c5744918690a1"
};

firebase.initializeApp(firebaseConfig);

let app;

firebase.auth().onAuthStateChanged(()=>{
  if(!app){
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})


